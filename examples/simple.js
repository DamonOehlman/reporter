var events = require('events'),
    reporter = require('../'),
    target = new events.EventEmitter();

// make the reporter watch the target
reporter.watch(target);

// add some rules to the reporter to handle click events
reporter.addRules({
    click: function() {
        console.log('A click happened');
    }
});

// tell the target to emit a click event
target.emit('click');