var assert = require('assert'),
    events = require('events'),
    reporter = require('../'),
    target = new events.EventEmitter();

describe('reporter tests, ensuring results are passed on', function() {
    it('should be able to capture events from a target', function(done) {
        var clicked = false;
        
        reporter.watch(target);
        reporter.addRules({
            click: function() {
                clicked = true;
            }
        });
        
        // bind the click handler for the target
        target.once('click', function() {
            assert(clicked);
            reporter.release(target);
            
            done();
        });
        
        target.emit('click');
    });
});

