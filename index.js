var _ = require('underscore');

function Reporter() {
    this.handlers = {};
}

Reporter.prototype = {
    addRules: function(handlers) {
        _.extend(this.handlers, handlers || {});
    },
    
    watch: function(target) {
        var origEmit = (target || {}).emit,
            reporter = this;
        
        if (typeof origEmit == 'function') {
            target.emit = function(evtName) {
                var args = Array.prototype.slice.call(arguments, 1),
                    result;
                    
                // if we have a rule handler that matches the event name, then invoke it
                if (typeof reporter.handlers[evtName] == 'function') {
                    result = reporter.handlers[evtName].apply(target, args);
                }
                
                // if it returns a undefined result or truthy result, then invoke the original handler
                if (typeof result == 'undefined' || result) {
                    origEmit.apply(target, arguments);
                }
            };
        }
    },
    
    release: function(target) {
        // delete the emit function from the target, which means the prototype handler will take over
        delete target.emit;
    }
};

module.exports = new Reporter();